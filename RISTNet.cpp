//
// Created by Anders Cedronius (Edgeware AB) on 2020-03-14.
//

#include "RISTNet.h"
#include "RISTNetInternal.h"

//---------------------------------------------------------------------------------------------------------------------
//
//
// RIST Network tools
//
//
//---------------------------------------------------------------------------------------------------------------------

bool RISTNetTools::isIPv4(const std::string &rStr) {
    struct sockaddr_in lsa;
    return inet_pton(AF_INET, rStr.c_str(), &(lsa.sin_addr)) != 0;
}

bool RISTNetTools::isIPv6(const std::string &rStr) {
    struct sockaddr_in6 lsa;
    return inet_pton(AF_INET6, rStr.c_str(), &(lsa.sin6_addr)) != 0;
}

bool RISTNetTools::buildRISTURL(std::string lIP, std::string lPort, std::string &rURL, bool lListen) {
    int lIPType;
    if (isIPv4(lIP)) {
        lIPType = AF_INET;
    } else if (isIPv6(lIP)) {
        lIPType = AF_INET6;
    } else {
        LOGGER(true, LOGG_ERROR, " " << "Provided IP-Address not valid.")
        return false;
    }
    int lPortNum = 0;
    std::stringstream lPortNumStr(lPort);
    lPortNumStr >> lPortNum;
    if (lPortNum < 1 || lPortNum > INT16_MAX) {
        LOGGER(true, LOGG_ERROR, " " << "Provided Port number not valid.")
        return false;
    }
    std::string lRistURL = "";
    if (lIPType == AF_INET) {
        lRistURL += "rist://";
    } else {
        lRistURL += "rist6://";
    }
    if (lListen) {
        lRistURL += "@";
    }
    if (lIPType == AF_INET) {
        lRistURL += lIP + ":" + lPort;
    } else {
        lRistURL += "[" + lIP + "]:" + lPort;
    }
    rURL = lRistURL;
    return true;
}

//---------------------------------------------------------------------------------------------------------------------
//
//
// RISTNetReceiver  --  RECEIVER
//
//
//---------------------------------------------------------------------------------------------------------------------

RISTNetReceiver::RISTNetReceiver() {
    // Set the callback stubs
    validateConnectionCallback = std::bind(&RISTNetReceiver::validateConnectionStub, this, std::placeholders::_1,
                                           std::placeholders::_2);
    networkDataCallback = std::bind(&RISTNetReceiver::dataFromClientStub, this, std::placeholders::_1,
                                    std::placeholders::_2, std::placeholders::_3);
    LOGGER(false, LOGG_NOTIFY, "RISTNetReceiver constructed")
}

RISTNetReceiver::~RISTNetReceiver() {
    if (mRistReceiver) {
        int lStatus = rist_receiver_destroy(mRistReceiver);
        if (lStatus) {
            LOGGER(true, LOGG_ERROR, "rist_receiver_destroy failure")
        }
    }
    LOGGER(false, LOGG_NOTIFY, "RISTNetReceiver destruct")
}

//---------------------------------------------------------------------------------------------------------------------
// RISTNetReceiver  --  Callbacks --- Start
//---------------------------------------------------------------------------------------------------------------------


std::shared_ptr<NetworkConnection> RISTNetReceiver::validateConnectionStub(std::string lIPAddress, uint16_t lPort) {
    LOGGER(true, LOGG_ERROR,
           "validateConnectionCallback not implemented. Will not accept connection from: " << lIPAddress << ":"
                                                                                           << unsigned(lPort))
    return nullptr;
}

void RISTNetReceiver::dataFromClientStub(const uint8_t *pBuf, size_t lSize,
                                         std::shared_ptr<NetworkConnection> &rConnection) {
    LOGGER(true, LOGG_ERROR, "networkDataCallback not implemented. Data is lost")
}

int RISTNetReceiver::receiveData(void *pArg, const rist_data_block *pDataBlock) {
    RISTNetReceiver *lWeakSelf = (RISTNetReceiver *) pArg;
    lWeakSelf->mClientListMtx.lock();
    auto netObj = lWeakSelf->mClientList.find(pDataBlock->peer);
    if (netObj != lWeakSelf->mClientList.end()) {
        auto netCon = netObj->second;
        lWeakSelf->mClientListMtx.unlock();
        lWeakSelf->networkDataCallback((const uint8_t *) pDataBlock->payload, pDataBlock->payload_len, netCon, pDataBlock->peer, pDataBlock->flow_id);
        return 0;
    } else {
        LOGGER(true, LOGG_ERROR, "receivesendDataData mClientList <-> peer mismatch.")
    }
    lWeakSelf->mClientListMtx.unlock();
    return 0;
}

int RISTNetReceiver::receiveOOBData(void *pArg, const rist_oob_block *pOOBBlock) {
    RISTNetReceiver *lWeakSelf = (RISTNetReceiver *) pArg;
    if (lWeakSelf->networkOOBDataCallback) {  //This is a optional callback
        lWeakSelf->mClientListMtx.lock();
        auto netObj = lWeakSelf->mClientList.find(pOOBBlock->peer);
        if (netObj != lWeakSelf->mClientList.end()) {
            auto netCon = netObj->second;
            lWeakSelf->mClientListMtx.unlock();
            lWeakSelf->networkOOBDataCallback((const uint8_t *) pOOBBlock->payload, pOOBBlock->payload_len, netCon, pOOBBlock->peer);
            return 0;
        }
        lWeakSelf->mClientListMtx.unlock();
    }
    return 0;
}


int RISTNetReceiver::clientConnect(void *pArg, const char* pConnectingIP, uint16_t lConnectingPort, const char* pIP, uint16_t lPort, struct rist_peer *pPeer) {
    RISTNetReceiver *lWeakSelf = (RISTNetReceiver *) pArg;
    auto lNetObj = lWeakSelf->validateConnectionCallback(std::string(pConnectingIP), lConnectingPort);
    if (lNetObj) {
        lWeakSelf->mClientListMtx.lock();
        lWeakSelf->mClientList[pPeer] = lNetObj;
        lWeakSelf->mClientListMtx.unlock();
        return 1; // Accept the connection
    }
    return 0; // Reject the connection
}

int RISTNetReceiver::clientDisconnect(void *pArg, struct rist_peer *pPeer) {
    RISTNetReceiver *lWeakSelf = (RISTNetReceiver *) pArg;
    lWeakSelf->mClientListMtx.lock();
    if (lWeakSelf->mClientList.find(pPeer) == lWeakSelf->mClientList.end()) {
        LOGGER(true, LOGG_ERROR, "RISTNetReceiver::clientDisconnect unknown peer")
        lWeakSelf->mClientListMtx.unlock();
        return 0;
    } else {
        lWeakSelf->mClientList.erase(lWeakSelf->mClientList.find(pPeer)->first);
    }
    lWeakSelf->mClientListMtx.unlock();
    return 0;
}

//---------------------------------------------------------------------------------------------------------------------
// RISTNetReceiver  --  Callbacks --- End
//---------------------------------------------------------------------------------------------------------------------

void RISTNetReceiver::getActiveClients(
        std::function<void(std::map<struct rist_peer *, std::shared_ptr<NetworkConnection>> &)> lFunction) {
    mClientListMtx.lock();
    if (lFunction) {
        lFunction(mClientList);
    }
    mClientListMtx.unlock();
}

bool RISTNetReceiver::closeClientConnection(struct rist_peer *lPeer) {
    mClientListMtx.lock();
    auto netObj = mClientList.find(lPeer);
    if (netObj == mClientList.end()) {
        LOGGER(true, LOGG_ERROR, "Could not find peer")
        mClientListMtx.unlock();
        return false;
    }
    mClientList.erase(lPeer);
    mClientListMtx.unlock();
    int lStatus = rist_receiver_peer_destroy(mRistReceiver, lPeer);
    if (lStatus) {
        LOGGER(true, LOGG_ERROR, "rist_receiver_peer_destroy failed: ")
        return false;
    }
    return true;
}

void RISTNetReceiver::closeAllClientConnections() {
    mClientListMtx.lock();
    for (auto &rPeer: mClientList) {
        struct rist_peer *lPeer = rPeer.first;
        int lStatus = rist_receiver_peer_destroy(mRistReceiver, lPeer);
        if (lStatus) {
            LOGGER(true, LOGG_ERROR, "rist_receiver_peer_destroy failed: ")
        }
    }
    mClientList.clear();
    mClientListMtx.unlock();
}

bool RISTNetReceiver::destroyReceiver() {
    if (mRistReceiver) {
        int lStatus = rist_receiver_destroy(mRistReceiver);
        mRistReceiver = nullptr;
        mClientListMtx.lock();
        mClientList.clear();
        mClientListMtx.unlock();
        if (lStatus) {
            LOGGER(true, LOGG_ERROR, "rist_receiver_destroy fail.")
            return false;
        }
    } else {
        LOGGER(true, LOGG_WARN, "RIST receiver not initialised.")
        return false;
    }
    return true;
}

bool RISTNetReceiver::initReceiver(std::vector<std::string> &rURLList,
                                   RISTNetReceiver::RISTNetReceiverSettings &rSettings) {
    if (!rURLList.size()) {
        LOGGER(true, LOGG_ERROR, "URL list is empty.")
        return false;
    }

    int lStatus;
    lStatus = rist_receiver_create(&mRistReceiver, rSettings.mProfile, rSettings.mLogLevel);
    if (lStatus) {
        LOGGER(true, LOGG_ERROR, "rist_receiver_create fail.")
        return false;
    }
    for (auto &rURL: rURLList) {
        int keysize = 0;
        if (rSettings.mPSK.size()) {
            keysize = 128;
        }
        mRistPeerConfig.version = RIST_PEER_CONFIG_VERSION;
        mRistPeerConfig.virt_dst_port = RIST_DEFAULT_VIRT_DST_PORT;
        mRistPeerConfig.recovery_mode = rSettings.mPeerConfig.recovery_mode;
        mRistPeerConfig.recovery_maxbitrate = rSettings.mPeerConfig.recovery_maxbitrate;
        mRistPeerConfig.recovery_maxbitrate_return = rSettings.mPeerConfig.recovery_maxbitrate_return;
        mRistPeerConfig.recovery_length_min = rSettings.mPeerConfig.recovery_length_min;
        mRistPeerConfig.recovery_length_max = rSettings.mPeerConfig.recovery_length_max;
        mRistPeerConfig.recovery_rtt_min = rSettings.mPeerConfig.recovery_rtt_min;
        mRistPeerConfig.recovery_rtt_max = rSettings.mPeerConfig.recovery_rtt_max;
        mRistPeerConfig.weight = 5;
        mRistPeerConfig.buffer_bloat_mode = rSettings.mPeerConfig.buffer_bloat_mode;
        mRistPeerConfig.buffer_bloat_limit = rSettings.mPeerConfig.buffer_bloat_limit;
        mRistPeerConfig.buffer_bloat_hard_limit = rSettings.mPeerConfig.buffer_bloat_hard_limit;
        mRistPeerConfig.session_timeout = rSettings.mSessionTimeout;
        mRistPeerConfig.keepalive_interval =  rSettings.mKeepAliveInterval;
        mRistPeerConfig.key_size = keysize;

        if (keysize) {
            strncpy((char *) &mRistPeerConfig.secret[0], rSettings.mPSK.c_str(), 128);
        }

        if (rSettings.mCNAME.size()) {
            strncpy((char *) &mRistPeerConfig.cname[0], rSettings.mCNAME.c_str(), 128);
        }

        lStatus = rist_parse_address(rURL.c_str(), (const rist_peer_config **)&mRistPeerConfig);
        if (lStatus)
        {
            LOGGER(true, LOGG_ERROR, "rist_parse_address fail: " << rURL)
            destroyReceiver();
            return false;
        }

        struct rist_peer *peer;
        lStatus =  rist_receiver_peer_create(mRistReceiver, &peer, &mRistPeerConfig);
        if (lStatus) {
            LOGGER(true, LOGG_ERROR, "rist_receiver_peer_create fail: " << rURL)
            destroyReceiver();
            return false;
        }
    }

    if (rSettings.mMaxjitter) {
        lStatus = rist_receiver_jitter_max_set(mRistReceiver, rSettings.mMaxjitter);
        if (lStatus) {
            LOGGER(true, LOGG_ERROR, "rist_receiver_jitter_max_set fail.")
            destroyReceiver();
            return false;
        }
    }

    lStatus = rist_receiver_oob_set(mRistReceiver, receiveOOBData, this);
    if (lStatus) {
        LOGGER(true, LOGG_ERROR, "rist_receiver_oob_set fail.")
        destroyReceiver();
        return false;
    }

    lStatus = rist_receiver_data_callback_set(mRistReceiver, receiveData, this);
    if (lStatus) {
        LOGGER(true, LOGG_ERROR, "rist_receiver_data_callback_set fail.")
        destroyReceiver();
        return false;
    }

    lStatus = rist_receiver_auth_handler_set(mRistReceiver, clientConnect, clientDisconnect, this);
    if (lStatus) {
        LOGGER(true, LOGG_ERROR, "rist_receiver_auth_handler_set fail.")
        destroyReceiver();
        return false;
    }

    lStatus = rist_receiver_start(mRistReceiver);
    if (lStatus) {
        LOGGER(true, LOGG_ERROR, "rist_receiver_start fail.")
        destroyReceiver();
        return false;
    }
    return true;
}

bool RISTNetReceiver::sendOOBData(struct rist_peer *pPeer, const uint8_t *pData, size_t lSize) {
    if (!mRistReceiver) {
        LOGGER(true, LOGG_ERROR, "RISTNetReceiver not initialised.")
        return false;
    }

    rist_oob_block myOOBBlock = {0};
    myOOBBlock.peer = pPeer;
    myOOBBlock.payload = pData;
    myOOBBlock.payload_len = lSize;

    int lStatus = rist_receiver_oob_write(mRistReceiver, &myOOBBlock);
    if (lStatus) {
        LOGGER(true, LOGG_ERROR, "rist_receiver_oob_write failed.")
        destroyReceiver();
        return false;
    }
    return true;
}

void RISTNetReceiver::getVersion(uint32_t &rCppWrapper, uint32_t &rRistMajor, uint32_t &rRistMinor) {
    rCppWrapper = CPP_WRAPPER_VERSION;
    rRistMajor = RIST_PROTOCOL_VERSION;
    rRistMinor = RIST_SUBVERSION;
}

//---------------------------------------------------------------------------------------------------------------------
//
//
// RISTNetSender  --  SENDER
//
//
//---------------------------------------------------------------------------------------------------------------------

RISTNetSender::RISTNetSender() {
    validateConnectionCallback = std::bind(&RISTNetSender::validateConnectionStub, this, std::placeholders::_1,
                                           std::placeholders::_2);
    LOGGER(false, LOGG_NOTIFY, "RISTNetSender constructed")
}

RISTNetSender::~RISTNetSender() {
    if (mRistSender) {
        int lStatus = rist_sender_destroy(mRistSender);
        if (lStatus) {
            LOGGER(true, LOGG_ERROR, "rist_sender_destroy fail.")
        }
    }
    LOGGER(false, LOGG_NOTIFY, "RISTNetClient destruct.")
}

//---------------------------------------------------------------------------------------------------------------------
// RISTNetSender  --  Callbacks --- Start
//---------------------------------------------------------------------------------------------------------------------

std::shared_ptr<NetworkConnection> RISTNetSender::validateConnectionStub(std::string ipAddress, uint16_t port) {
    LOGGER(true, LOGG_ERROR,
           "validateConnectionCallback not implemented. Will not accept connection from: " << ipAddress << ":"
                                                                                           << unsigned(port))
    return 0;
}

int RISTNetSender::receiveOOBData(void *pArg, const rist_oob_block *pOOBBlock) {
    RISTNetSender *lWeakSelf = (RISTNetSender *) pArg;
    if (lWeakSelf->networkOOBDataCallback) {  //This is a optional callback
        lWeakSelf->mClientListMtx.lock();
        auto netObj = lWeakSelf->mClientList.find(pOOBBlock->peer);
        if (netObj != lWeakSelf->mClientList.end()) {
            auto netCon = netObj->second;
            lWeakSelf->mClientListMtx.unlock();
            lWeakSelf->networkOOBDataCallback((const uint8_t *) pOOBBlock->payload, pOOBBlock->payload_len, netCon, pOOBBlock->peer);
            return 0;
        }
        lWeakSelf->mClientListMtx.unlock();
    }
    return 0;
}

int RISTNetSender::clientConnect(void *pArg, const char* pConnectingIP, uint16_t lConnectingPort, const char* pIP, uint16_t lPort, struct rist_peer *pPeer) {
    RISTNetSender *lWeakSelf = (RISTNetSender *) pArg;
    auto lNetObj = lWeakSelf->validateConnectionCallback(std::string(pConnectingIP), lConnectingPort);
    if (lNetObj) {
        lWeakSelf->mClientListMtx.lock();
        lWeakSelf->mClientList[pPeer] = lNetObj;
        lWeakSelf->mClientListMtx.unlock();
        return 1; // Accept the connection
    }
    return 0; // Reject the connection
}

int RISTNetSender::clientDisconnect(void *pArg, struct rist_peer *pPeer) {
    RISTNetSender *lWeakSelf = (RISTNetSender *) pArg;
    lWeakSelf->mClientListMtx.lock();
    if (lWeakSelf->mClientList.find(pPeer) == lWeakSelf->mClientList.end()) {
        LOGGER(true, LOGG_ERROR, "RISTNetReceiver::clientDisconnect unknown peer")
        lWeakSelf->mClientListMtx.unlock();
        return 0;
    } else {
        lWeakSelf->mClientList.erase(lWeakSelf->mClientList.find(pPeer)->first);
    }
    lWeakSelf->mClientListMtx.unlock();
    return 0;
}

//---------------------------------------------------------------------------------------------------------------------
// RISTNetSender  --  Callbacks --- End
//---------------------------------------------------------------------------------------------------------------------

void RISTNetSender::getActiveClients(
        std::function<void(std::map<struct rist_peer *, std::shared_ptr<NetworkConnection>> &)> lFunction) {
    mClientListMtx.lock();
    if (lFunction) {
        lFunction(mClientList);
    }
    mClientListMtx.unlock();
}

bool RISTNetSender::closeClientConnection(struct rist_peer *lPeer) {
    mClientListMtx.lock();
    auto netObj = mClientList.find(lPeer);
    if (netObj == mClientList.end()) {
        LOGGER(true, LOGG_ERROR, "Could not find peer")
        mClientListMtx.unlock();
        return false;
    }
    mClientList.erase(lPeer);
    mClientListMtx.unlock();
    int lStatus = rist_sender_peer_destroy(mRistSender, lPeer);
    if (lStatus) {
        LOGGER(true, LOGG_ERROR, "rist_sender_peer_destroy failed: ")
        return false;
    }
    return true;
}

void RISTNetSender::closeAllClientConnections() {
    mClientListMtx.lock();
    for (auto &rPeer: mClientList) {
        struct rist_peer *pPeer = rPeer.first;
        int status = rist_sender_peer_destroy(mRistSender, pPeer);
        if (status) {
            LOGGER(true, LOGG_ERROR, "rist_sender_peer_destroy failed: ")
        }
    }
    mClientList.clear();
    mClientListMtx.unlock();
}

bool RISTNetSender::destroySender() {
    if (mRistSender) {
        int lStatus = rist_sender_destroy(mRistSender);
        mRistSender = nullptr;
        mClientListMtx.lock();
        mClientList.clear();
        mClientListMtx.unlock();
        if (lStatus) {
            LOGGER(true, LOGG_ERROR, "rist_sender_destroy fail.")
            return false;
        }
    } else {
        LOGGER(true, LOGG_WARN, "RIST Sender not running.")
    }
    return true;
}

bool RISTNetSender::initSender(std::vector<std::tuple<std::string,int>> &rPeerList,
                               RISTNetSenderSettings &rSettings) {

    if (!rPeerList.size()) {
        LOGGER(true, LOGG_ERROR, "URL list is empty.")
        return false;
    }

    int lStatus;
    lStatus = rist_sender_create(&mRistSender, rSettings.mProfile, 0, rSettings.mLogLevel);
    if (lStatus) {
        LOGGER(true, LOGG_ERROR, "rist_sender_create fail.")
        return false;
    }
    for (auto &rPeerInfo: rPeerList) {

        auto peerURL = std::get<0>(rPeerInfo);

        int keysize = 0;
        if (rSettings.mPSK.size()) {
            keysize = 128;
        }
        mRistPeerConfig.version = RIST_PEER_CONFIG_VERSION;
        mRistPeerConfig.virt_dst_port = RIST_DEFAULT_VIRT_DST_PORT;
        mRistPeerConfig.recovery_mode = rSettings.mPeerConfig.recovery_mode;
        mRistPeerConfig.recovery_maxbitrate = rSettings.mPeerConfig.recovery_maxbitrate;
        mRistPeerConfig.recovery_maxbitrate_return = rSettings.mPeerConfig.recovery_maxbitrate_return;
        mRistPeerConfig.recovery_length_min = rSettings.mPeerConfig.recovery_length_min;
        mRistPeerConfig.recovery_length_max = rSettings.mPeerConfig.recovery_length_max;
        mRistPeerConfig.recovery_rtt_min = rSettings.mPeerConfig.recovery_rtt_min;
        mRistPeerConfig.recovery_rtt_max = rSettings.mPeerConfig.recovery_rtt_max;
        mRistPeerConfig.weight = std::get<1>(rPeerInfo);
        mRistPeerConfig.buffer_bloat_mode = rSettings.mPeerConfig.buffer_bloat_mode;
        mRistPeerConfig.buffer_bloat_limit = rSettings.mPeerConfig.buffer_bloat_limit;
        mRistPeerConfig.buffer_bloat_hard_limit = rSettings.mPeerConfig.buffer_bloat_hard_limit;
        mRistPeerConfig.session_timeout = rSettings.mSessionTimeout;
        mRistPeerConfig.keepalive_interval =  rSettings.mKeepAliveInterval;
        mRistPeerConfig.key_size = keysize;

        if (keysize) {
            strncpy((char *) &mRistPeerConfig.secret[0], rSettings.mPSK.c_str(), 128);
        }

        if (rSettings.mCNAME.size()) {
            strncpy((char *) &mRistPeerConfig.cname[0], rSettings.mCNAME.c_str(), 128);
        }

        lStatus = rist_parse_address(peerURL.c_str(), (const rist_peer_config **)&mRistPeerConfig);
        if (lStatus)
        {
            LOGGER(true, LOGG_ERROR, "rist_parse_address fail: " << peerURL)
            destroySender();
            return false;
        }

        struct rist_peer *peer;
        lStatus =  rist_sender_peer_create(mRistSender, &peer, &mRistPeerConfig);
        if (lStatus) {
            LOGGER(true, LOGG_ERROR, "rist_sender_peer_create fail: " << peerURL)
            destroySender();
            return false;
        }
    }

    if (rSettings.mMaxJitter) {
        lStatus = rist_sender_jitter_max_set(mRistSender, rSettings.mMaxJitter);
        if (lStatus) {
            LOGGER(true, LOGG_ERROR, "rist_sender_jitter_max_set fail.")
            destroySender();
            return false;
        }
    }

    lStatus = rist_sender_oob_set(mRistSender, receiveOOBData, this);
    if (lStatus) {
        LOGGER(true, LOGG_ERROR, "rist_sender_oob_set fail.")
        destroySender();
        return false;
    }

    lStatus = rist_sender_auth_handler_set(mRistSender, clientConnect, clientDisconnect, this);
    if (lStatus) {
        LOGGER(true, LOGG_ERROR, "rist_sender_auth_handler_set fail.")
        destroySender();
        return false;
    }

    lStatus = rist_sender_start(mRistSender);
    if (lStatus) {
        LOGGER(true, LOGG_ERROR, "rist_sender_start fail.")
        destroySender();
        return false;
    }

    return true;
}

bool RISTNetSender::sendData(const uint8_t *pData, size_t lSize, uint16_t lConnectionID) {
    if (!mRistSender) {
        LOGGER(true, LOGG_ERROR, "RISTNetSender not initialised.")
        return false;
    }

    rist_data_block myRISTDataBlock = {0};
    myRISTDataBlock.payload = pData;
    myRISTDataBlock.payload_len = lSize;
    myRISTDataBlock.flow_id = lConnectionID;

    int lStatus = rist_sender_data_write(mRistSender, &myRISTDataBlock);
    if (lStatus) {
        LOGGER(true, LOGG_ERROR, "rist_client_write failed.")
        destroySender();
        return false;
    }
    return true;
}

bool RISTNetSender::sendOOBData(struct rist_peer *pPeer, const uint8_t *pData, size_t lSize) {
    if (!mRistSender) {
        LOGGER(true, LOGG_ERROR, "RISTNetSender not initialised.")
        return false;
    }

    rist_oob_block myOOBBlock = {0};
    myOOBBlock.peer = pPeer;
    myOOBBlock.payload = pData;
    myOOBBlock.payload_len = lSize;

    int lStatus = rist_sender_oob_write(mRistSender, &myOOBBlock);
    if (lStatus) {
        LOGGER(true, LOGG_ERROR, "rist_sender_oob_write failed.")
        destroySender();
        return false;
    }
    return true;
}

void RISTNetSender::getVersion(uint32_t &rCppWrapper, uint32_t &rRistMajor, uint32_t &rRistMinor) {
    rCppWrapper = CPP_WRAPPER_VERSION;
    rRistMajor = RIST_PROTOCOL_VERSION;
    rRistMinor = RIST_SUBVERSION;
}
