//
// Created by Anders Cedronius (Edgeware AB) on 2020-03-14.
//

// Prefixes used
// m class member
// p pointer (*)
// r reference (&)
// l local scope

#ifndef CPPRISTWRAPPER__RISTNET_H
#define CPPRISTWRAPPER__RISTNET_H

#define CPP_WRAPPER_VERSION 21

#include "librist.h"
#include <string.h>
#include <sys/time.h>
#include <any>
#include <tuple>
#include <vector>
#include <sstream>
#include <memory>
#include <atomic>
#include <map>
#include <functional>
#include <mutex>
#include <sys/socket.h>
#include <arpa/inet.h>

 /**
  * \class NetworkConnection
  *
  * \brief
  *
  * A NetworkConnection class is the maintainer and carrier of the user class passed to the connection.
  *
  */
class NetworkConnection {
public:
  std::any mObject; //Contains your object
};

/**
 * \class RISTNetTools
 *
 * \brief
 *
 * A helper class for the RIST C++ wrapper
 *
 */
class RISTNetTools {
public:
  ///Build the librist url based on name/ip, port and if it's a listen or not peer
  bool buildRISTURL(std::string lIP, std::string lPort, std::string &rURL, bool lListen);
private:
  bool isIPv4(const std::string &rStr);
  bool isIPv6(const std::string &rStr);
};

//---------------------------------------------------------------------------------------------------------------------
//
//
// RISTNetReceiver  --  RECEIVER
//
//
//---------------------------------------------------------------------------------------------------------------------

 /**
  * \class RISTNetReceiver
  *
  * \brief
  *
  * A RISTNetReceiver receives data from a sender. The reciever can listen for a sender to connect
  * or connect to a sender that listens.
  *
  */
class RISTNetReceiver {
public:

  struct RISTNetReceiverSettings {
    enum rist_profile mProfile = RIST_PROFILE_MAIN;
    rist_peer_config mPeerConfig = {
            .version = RIST_PEER_CONFIG_VERSION,
            .virt_dst_port = RIST_DEFAULT_VIRT_DST_PORT,
            .recovery_mode = RIST_DEFAULT_RECOVERY_MODE,
            .recovery_maxbitrate = RIST_DEFAULT_RECOVERY_MAXBITRATE,
            .recovery_maxbitrate_return = RIST_DEFAULT_RECOVERY_MAXBITRATE_RETURN,
            .recovery_length_min = RIST_DEFAULT_RECOVERY_LENGHT_MIN,
            .recovery_length_max = RIST_DEFAULT_RECOVERY_LENGHT_MAX,
            .recovery_reorder_buffer = RIST_DEFAULT_RECOVERY_REORDER_BUFFER,
            .recovery_rtt_min = RIST_DEFAULT_RECOVERY_RTT_MIN,
            .recovery_rtt_max = RIST_DEFAULT_RECOVERY_RTT_MAX,
            .weight = 5,
            .buffer_bloat_mode = RIST_DEFAULT_BUFFER_BLOAT_MODE,
            .buffer_bloat_limit = RIST_DEFAULT_BUFFER_BLOAT_LIMIT,
            .buffer_bloat_hard_limit = RIST_DEFAULT_BUFFER_BLOAT_HARD_LIMIT,
            .session_timeout = 10000
    };

    enum rist_log_level mLogLevel = RIST_LOG_QUIET;
    std::string mPSK = "";
    std::string mCNAME = "";
    int mSessionTimeout = 0;
    int mKeepAliveInterval = 0;
    int mMaxjitter = 0;
  };

  /// Constructor
  RISTNetReceiver();

  /// Destructor
  virtual ~RISTNetReceiver();

  /**
   * @brief Initialize receiver
   *
   * Initialize the receiver using the provided settings and parameters.
   *
   * @param rURLList is a vector of RIST formated URL's
   * @param The receiver settings
   * @return true on success
   */
  bool initReceiver(std::vector<std::string> &rURLList,
                    RISTNetReceiverSettings &rSettings);

  /**
   * @brief Map of all active connections
   *
   * Get a map of all connected clients
   *
   * @param function getting the map of active clients (normally a lambda).
   */
  void getActiveClients(std::function<void(std::map<struct rist_peer *,
                                                    std::shared_ptr<NetworkConnection>> &)> function);

  /**
   * @brief Close a client connection
   *
   * Closes a client connection.
   *
   */
  bool closeClientConnection(struct rist_peer *);


  /**
   * @brief Close all active connections
   *
   * Closes all active connections.
   *
   */
  void closeAllClientConnections();

  /**
   * @brief Send OOB data (Currently not working in librist)
   *
   * Sends OOB data to the specified peer
   * OOB data is encrypted (if used) but not protected for network loss
   *
   * @param target peer
   * @param pointer to the data
   * @param length of the data
   *
   */
  bool sendOOBData(struct rist_peer *pPeer ,const uint8_t *pData, size_t lSize);

  /**
   * @brief Destroys the receiver
   *
   * Destroys the receiver and garbage collects all underlying assets.
   *
   */
  bool destroyReceiver();

  /**
   * @brief Gets the version
   *
   * Gets the version of the C++ librist wrapper sender and librist
   *
   * @return The cpp wrapper version, rist major and minor version.
   */
  void getVersion(uint32_t &rCppWrapper, uint32_t &rRistMajor, uint32_t &rRistMinor);

  //To be implemented
  //void getInfo();

  /**
   * @brief Data receive callback
   *
   * When receiving data from the sender this function is called.
   * You get a pointer to the data, the length and the NetworkConnection object containing your
   * object if you did put a object there. The sender can also put a optional uint16_t value (not 0) associated with the data
   *
   * @param function getting data from the sender.
   */
  std::function<void(const uint8_t *pBuf, size_t lSize, std::shared_ptr<NetworkConnection> &rConnection, struct rist_peer *pPeer, uint16_t lConnectionID)>
      networkDataCallback = nullptr;

  /**
   * @brief OOB Data receive callback (__NULLABLE)
   *
   * When receiving data from the sender this function is called.
   * You get a pointer to the data, the length and the NetworkConnection object containing your
   * object if you did put a object there.
   *
   * @param function getting data from the sender.
   */
  std::function<void(const uint8_t *pBuf, size_t lSize, std::shared_ptr<NetworkConnection> &rConnection, struct rist_peer *pPeer)>
      networkOOBDataCallback = nullptr;

  /**
   * @brief Validate connection callback
   *
   * If the reciever is in listen mode and a sender connects this method is called
   * Return a NetworkConnection if you want to accept this connection
   * You can attach any object to the NetworkConnection and the NetworkConnection object
   * will manage your objects lifecycle. Meaning it will release it when the connection
   * is terminated.
   *
   * @param function validating the connection.
   * @return a NetworkConnection object or nullptr for rejecting.
   */
  std::function<std::shared_ptr<NetworkConnection>(std::string lIPAddress, uint16_t lPort)>
      validateConnectionCallback = nullptr;

  // Delete copy and move constructors and assign operators
  RISTNetReceiver(RISTNetReceiver const &) = delete;             // Copy construct
  RISTNetReceiver(RISTNetReceiver &&) = delete;                  // Move construct
  RISTNetReceiver &operator=(RISTNetReceiver const &) = delete;  // Copy assign
  RISTNetReceiver &operator=(RISTNetReceiver &&) = delete;       // Move assign

private:

  std::shared_ptr<NetworkConnection> validateConnectionStub(std::string lIPAddress, uint16_t lPort);
  void dataFromClientStub(const uint8_t *pBuf, size_t lSize, std::shared_ptr<NetworkConnection> &rConnection);

  // Private method receiving the data from librist C-API
  static int receiveData(void *pArg, const rist_data_block *data_block);

  // Private method receiving OOB data from librist C-API
  static int receiveOOBData(void *pArg, const rist_oob_block *pOOB_block);

  // Private method called when a client connects
  static int clientConnect(void *pArg, const char* pConnectingIP, uint16_t lConnectingPort, const char* pIP, uint16_t lPort, struct rist_peer *pPeer);

  // Private method called when a client disconnects
  static int clientDisconnect(void *pArg, struct rist_peer *pPeer);

  // The context of a RIST receiver
  rist_receiver *mRistReceiver = nullptr;

  // The configuration of the RIST receiver
  rist_peer_config mRistPeerConfig = {0};

  // The mutex protecting the list. since the list can be accessed from both librist and the C++ layer
  std::mutex mClientListMtx;

  // The list of connected clients
  std::map<struct rist_peer *, std::shared_ptr<NetworkConnection>> mClientList;

  // Internal tools used by the C++ wrapper
  RISTNetTools mNetTools = RISTNetTools();
};

//---------------------------------------------------------------------------------------------------------------------
//
//
// RISTNetSender  --  SENDER
//
//
//---------------------------------------------------------------------------------------------------------------------

/**
 * \class RISTNetSender
 *
 * \brief
 *
 * A RISTNetSender sends data to a receiver. The sender can listen for a receiver to connect
 * or connect to a receiver that listens.
 *
 */
class RISTNetSender {
public:

  struct RISTNetSenderSettings {
    enum rist_profile mProfile = RIST_PROFILE_MAIN;
    rist_peer_config mPeerConfig = {
          .version = RIST_PEER_CONFIG_VERSION,
          .virt_dst_port = RIST_DEFAULT_VIRT_DST_PORT,
          .recovery_mode = RIST_DEFAULT_RECOVERY_MODE,
          .recovery_maxbitrate = RIST_DEFAULT_RECOVERY_MAXBITRATE,
          .recovery_maxbitrate_return = RIST_DEFAULT_RECOVERY_MAXBITRATE_RETURN,
          .recovery_length_min = RIST_DEFAULT_RECOVERY_LENGHT_MIN,
          .recovery_length_max = RIST_DEFAULT_RECOVERY_LENGHT_MAX,
          .recovery_reorder_buffer = RIST_DEFAULT_RECOVERY_REORDER_BUFFER,
          .recovery_rtt_min = RIST_DEFAULT_RECOVERY_RTT_MIN,
          .recovery_rtt_max = RIST_DEFAULT_RECOVERY_RTT_MAX,
          .weight = 5,
          .buffer_bloat_mode = RIST_DEFAULT_BUFFER_BLOAT_MODE,
          .buffer_bloat_limit = RIST_DEFAULT_BUFFER_BLOAT_LIMIT,
          .buffer_bloat_hard_limit = RIST_DEFAULT_BUFFER_BLOAT_HARD_LIMIT,
          .session_timeout = 10000
      };

    enum rist_log_level mLogLevel = RIST_LOG_QUIET;
    std::string mPSK = "";
    std::string mCNAME = "";
    uint32_t mSessionTimeout = 5000;
    uint32_t mKeepAliveInterval = 10000;
    int mMaxJitter = 0;
   };

  /// Constructor
  RISTNetSender();

  /// Destructor
  virtual ~RISTNetSender();

  /**
   * @brief Initialize sender
   *
   * Initialize the sender using the provided settings and parameters.
   *
   * @param rURLList is a vector of RIST formated URL's
   * @param The sender settings
   * @return true on success
   */
  bool initSender(std::vector<std::tuple<std::string,int>> &rPeerList,
                  RISTNetSenderSettings &rSettings);

  /**
   * @brief Map of all active connections
   *
   * Get a map of all connected clients
   *
   * @param function getting the map of active clients (normally a lambda).
   */
  void getActiveClients(std::function<void(std::map<struct rist_peer *,
                                                    std::shared_ptr<NetworkConnection>> &)> function);

  /**
   * @brief Close a client connection
   *
   * Closes a client connection.
   *
   */
  bool closeClientConnection(struct rist_peer *);

  /**
   * @brief Close all active connections
   *
   * Closes all active connections.
   *
   */
  void closeAllClientConnections();

  /**
   * @brief Send data
   *
   * Sends data to the connected peers
   *
   * @param pointer to the data
   * @param length of the data
   * @param a optional uint16_t value sent to the receiver
   *
   */
  bool sendData(const uint8_t *pData, size_t lSize, uint16_t lConnectionID=0);

  /**
  * @brief Send OOB data (Currently not working in librist)
  *
  * Sends OOB data to the specified peer
  * OOB data is encrypted (if used) but not protected for network loss
  *
  * @param target peer
  * @param pointer to the data
  * @param length of the data
  *
  */
  bool sendOOBData(struct rist_peer *pPeer ,const uint8_t *pData, size_t lSize);

  /**
   * @brief Destroys the sender
   *
   * Destroys the sender and garbage collects all underlying assets.
   *
   */
  bool destroySender();

  /**
   * @brief Gets the version
   *
   * Gets the version of the C++ librist wrapper sender and librist
   *
   * @return The cpp wrapper version, rist major and minor version.
   */
   void getVersion(uint32_t &rCppWrapper, uint32_t &rRistMajor, uint32_t &rRistMinor);

  //To be implemented
  //void getInfo();

  /**
   * @brief OOB Data receive callback (__NULLABLE)
   *
   * When receiving data from the sender this function is called.
   * You get a pointer to the data, the length and the NetworkConnection object containing your
   * object if you did put a object there.
   *
   * @param function getting data from the sender.
   */
  std::function<void(const uint8_t *pBuf, size_t lSize, std::shared_ptr<NetworkConnection> &rConnection, struct rist_peer *pPeer)>
      networkOOBDataCallback = nullptr;

  /**
   * @brief Validate connection callback
   *
   * If the sender is in listen mode and a receiver connects this method is called
   * Return a NetworkConnection if you want to accept this connection
   * You can attach any object to the NetworkConnection and the NetworkConnection object
   * will manage your objects lifecycle. Meaning it will release it when the connection
   * is terminated.
   *
   * @param function validating the connection.
   * @return a NetworkConnection object or nullptr for rejecting.
   */
  std::function<std::shared_ptr<NetworkConnection>(std::string lIPAddress, uint16_t lPort)>
      validateConnectionCallback = nullptr;

  // Delete copy and move constructors and assign operators
  RISTNetSender(RISTNetSender const &) = delete;             // Copy construct
  RISTNetSender(RISTNetSender &&) = delete;                  // Move construct
  RISTNetSender &operator=(RISTNetSender const &) = delete;  // Copy assign
  RISTNetSender &operator=(RISTNetSender &&) = delete;       // Move assign

private:

  std::shared_ptr<NetworkConnection> validateConnectionStub(std::string ipAddress, uint16_t port);
  void dataFromClientStub(const uint8_t *pBuf, size_t lSize, std::shared_ptr<NetworkConnection> &rConnection);

  // Private method receiving OOB data from librist C-API
  static int receiveOOBData(void *pArg, const rist_oob_block *pOOBBlock);

  // Private method called when a client connects
  static int clientConnect(void *pArg, const char* pConnectingIP, uint16_t lConnectingPort, const char* pIP, uint16_t lPort, struct rist_peer *pPeer);

  // Private method called when a client disconnects
  static int clientDisconnect(void *pArg, struct rist_peer *pPeer);

  // The context of a RIST sender
  rist_sender *mRistSender = nullptr;

  // The configuration of the RIST sender
  rist_peer_config mRistPeerConfig = {0};

  // The mutex protecting the list. since the list can be accessed from both librist and the C++ layer
  std::mutex mClientListMtx;

  // The list of connected clients
  std::map<struct rist_peer *, std::shared_ptr<NetworkConnection>> mClientList;

  // Internal tools used by the C++ wrapper
  RISTNetTools mNetTools = RISTNetTools();
};

#endif //CPPRISTWRAPPER__RISTNET_H
